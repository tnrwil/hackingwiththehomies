push_artifacts
=========

A brief description of the role goes here.

Requirements
------------
- create the qcow2 instance using qemu
- convert the qcow2 instance to vmdk
- copy the vmdk to s3
- copy the vmx file over (esxi 6.5) and create ova
- push ova and vmdk to s3
- push ova and vmdk to nexus

Role Variables
--------------
the vmx file can be templated out and here are those variables that would need to change
mem: '"4096"'
vmdk_name: '"rhel7_3.vmdk"'
display_name: '"rhel7_3"'
vm_name_nvram: '"rhel7_3.nvram"'
