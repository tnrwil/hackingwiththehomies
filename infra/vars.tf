variable "playbook_path"{
  default = "sanbox/infra/rhel_vm_provisioning.yaml"
}

variable "aws_region"{
  default = "us-east-1"
}

variable "aws_ami"{
  #default="ami-5a740e3b"
  #default="ami-1c81ce7d"
  default="ami-096fda3c22c1c990a"
}

variable "instance_count" {
  default="1"
}
variable "ec2_type" {
  default="c5.metal"
}
variable "ec2_key_name" {
  default="mytest-ebs"
}

variable "pub_key" {
  default="mytest-ebs.pem"
}

variable "pub_key_bucket_name" {
  default="cie-mis"
}

variable "ssh_user" {
  default="ec2-user"
}

variable "ebs_vol_size" {
  default="500"
}

#variable "private_subnet" {
#  default="subnet-ce688187"
#}
#
#variable "public_subnet" {
#  default="subnet-3492bd6e"
#}

variable "vpc" {
  default = "vpc-20e5f259"
}


variable "security_groups" {
  type    = list(string)
  default = ["sg-6ff7521c"]
}

