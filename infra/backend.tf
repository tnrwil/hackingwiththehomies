terraform {
backend "s3" {
  bucket = "cie-tfstate"
  key    = "gitlab/terraform.tfstate"
  region = "us-east-1"
  }
}

