# Log into AWS
provider "aws" {
  region = var.aws_region
}

data "aws_s3_bucket_object" "pub-key" {
  bucket = "${var.pub_key_bucket_name}"
  key = "${var.pub_key}"
}

resource "aws_instance" "HWTH-SERVER" {
  ami           = "${var.aws_ami}"
  instance_type = "${var.ec2_type}"
#  subnet_id = "${var.public_subnet}"
#  security_groups = "${var.security_groups}"
  key_name = "${var.ec2_key_name}"
  root_block_device {
    volume_size = "${var.ebs_vol_size}"
  }
  timeouts {
    create = "60m"
    update = "60m"
  }
  provisioner "remote-exec" {
    inline = [
      "sudo yum install python3 -y",
      "sudo yum install -y   python3-jinja2 python2-six python2-PyYAML python2-cryptography",
      "sudo rpm -ivh http://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm",
      "sudo yum install ansible -y",
      ]
      connection {
      type = "ssh"
      user = "${var.ssh_user}"
      private_key = "${file(var.pub_key)}"
      host = "${self.public_ip}"
      timeout = "10m"
      }
    }
   tags = {
    Name  = "HWTH"
    1067  = "N/A"
    Application = "security_pipeline"
    "Application Role" = "Service Management"
    Email = "tnr@womeninlinux.com"
    Environment = "DEV"
    OS = "RHEL_8"
    Organization = "WIL"
    Owner = "Tameika Reed"
    Project = "CI/CD"
  }
}

