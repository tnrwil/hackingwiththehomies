Puppet Agent Install
=========

Install puppet agents on all machines.

Requirements
------------
SSH, Ansible, and login credentials(root) to install the agent on the machine.

Role Variables
--------------
packages is the variable used to install the puppet agent on the machine

Dependencies
------------
ruby


License
-------

BSD

Author Information
------------------
Tameika and Dan
