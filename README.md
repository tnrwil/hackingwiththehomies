# Packer Example - Red Hat Enterprise Linux 8 Qemu type using Ansible provisioner

This example build configuration installs and configures RHEL 8 x86_64 using Ansible, and then generates two Vagrant box files, for:

  - Qemu using KVM
  - VMware

The example can be modified to use more Ansible roles, plays, and included playbooks to fully configure (or partially) configure a box file suitable for deployment for development environments.

## Requirements

The following software must be installed/present on your local machine before you can use Packer to build the Vagrant box file:

  - [Packer](http://www.packer.io/)
  - [KVM](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/5/html/virtualization/sect-virtualization-installing_the_virtualization_packages-installing_kvm_with_a_new_red_hat_enterprise_linux_installation)
  - [Ansible](http://docs.ansible.com/intro_installation.html)
  - [VMware OVFtool](https://code.vmware.com/web/tool/4.3.0/ovf)

You must have a Red Hat Subscription to download the Red Hat Enterprise Linux 7 iso. If you don't, [create an account](https://developers.redhat.com) and accept the terms and conditions of the Red Hat Developer Program, which provides no-cost subscriptions for development use only.

## Usage

  - Make sure all the required software (listed above) is installed.
  - cd to the directory containing this README.md file
  - Create `iso/` directory and move inside your Red Hat Enterprise Linux iso.
  - Edit ova-unstig.json file, check if `iso_urls` and `iso_checksum` match your RHEL iso. This is file is located in create_vm_from_iso
  - You can also set the checksum on the iso by running md5sum some.iso.

- Pipeline Explained:
```
    - setup_host
      Here the aws credentials, hosts (gitlab runner speaks to this host), download of pem files.

    - move_iso
      Download the ISO from the Redhat Satellite Server. In this case we are downloading RHEL 7.3 Server ISO.


    - create_unstig_ova
      Here we use the ova-unstig.json file to create an unstigged image. The image gets registered with the satellite server to get the latest packages installed. The eracent agent isinstalled. The machine is deregistered from the satellite server after the install of the agents. During the build of the RHEL8 box, a 'yum update' is launched. The VirtualBox Guest Additions installed are probably not the one for the newly installed kernel.

    - create_stig_ova
      Here we use the ova-stig.json file to create an stigged image. The image gets registered with the satellite server to get the latest packages installed. The eracent agent is also installed. The machine is deregistered from the satellite server after the install of the agents. During the build of the RHEL8 box, a 'yum update' is launched. The VirtualBox Guest Additions installed are probably not the one for the newly installed kernel.
   
    - push_artifacts
      After a few minutes, you should be able to check s3://cie-mis/VMDK and s3://cie-mis/OVA for the proper files and timestamp
      The OVA, VMDK, KS, and VMX, all reside in S3 bucket. S3://cie-mis/{VMX, OVA, VMDK}
    

    - remove_images
      After everything is built this playbook removes all of the images and any files. This is needed for the next run forfor qemu.

```


## Testing built boxes

###  Prerequisite

The following vagrant plugins should be installed:
 - Testing can be done by pushing the ova to S3
 - Deploying the ova to ESXi or VMDK with VMX to ESXi

 ### GitLab Environment variables

 The following environment variables need to be added under Settings > CI/CD > Environment variables in the GitLab CE GUI.
 - AWS_ACCESS_KEY_ID
 - AWS_SECRET_ACCESS_KEY
