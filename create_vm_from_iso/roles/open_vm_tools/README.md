OpenVmTools
=========
This installs openvm tools version open-vm-tools 10.0.5-2 . This is version needed for VRA


Requirements
------------
This is in the file section of this role


Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.


License
-------

BSD

Author Information
------------------
Tameika Reed
