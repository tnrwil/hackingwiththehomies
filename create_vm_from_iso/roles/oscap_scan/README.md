OSCAP SCAN
=========

This will do a eval, scan, remediate, and generate reports of the findings

Requirements
------------
Please ensure the following is installed
  - scap-workbench
  - openscap-scanner
  - scap-security-guide


Role Variables
--------------

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.


Examples
--------------
These are examples commands to run for information 

-  oscap -V (lets you know if the command is installed and give information about the tool)

Displaying information about OSCAP content
- oscap info /usr/share/xml/scap/ssg/content/ssg-rhel7-ds.xml

Scanning the System Using the SSG OVAL definitions
- oscap oval eval --results scan-oval-results.xml /usr/share/xml/scap/ssg/content/ssg-rhel7-ds.xml

Scanning the System Using the SSG XCCDF benchmark
- oscap xccdf eval --profile xccdf_org.ssgproject.content_profile_rht-ccp --results scan-xccdf-results.xml /usr/share/xml/scap/ssg/content/ssg-rhel7-ds.xml

Generating a Guide with a Checklist
-  oscap xccdf generate guide --profile xccdf_org.ssgproject.content_profile_rht-ccp /usr/share/xml/scap/ssg/content/ssg-rhel7-ds.xml > ssg-guide-checklist.html

Transforming an SSG OVAL Scan Result into a Report
-   oscap oval generate report scan-oval-results.xml > ssg-scan-oval-report.html

Transforming an SSG XCCDF Scan Result into a Report
-   oscap xccdf generate report scan-xccdf-results.xml > scan-xccdf-report.html

The result report is stored as the ssg-scan-xccdf-report.html file in the current directory. Alternatively, you can generate this report in the time of the scan using the --report command-line argument:
-   oscap xccdf eval --profile xccdf_org.ssgproject.content_profile_rht-ccp --results scan-xccdf-results.xml --report scan-xccdf-report.html /usr/share/xml/scap/ssg/content/ssg-rhel7-ds.xml

Validating SCAP Content
-   oscap ds sds-validate /usr/share/xml/scap/ssg/content/ssg-rhel7-ds.xml


With certain SCAP content, such as OVAL specification, you can also perform a Schematron validation. The Schematron validation is slower than the standard validation but provides deeper analysis, and is thus able to detect more errors. The following SSG example shows typical usage of the command

-  oscap oval validate --schematron /usr/share/xml/scap/ssg/content/ssg-rhel7-ds.xml


Using OpenSCAP to Remediate the System
-   oscap xccdf eval --remediate --profile xccdf_org.ssgproject.content_profile_rht-ccp --results scan-xccdf-results.xml /usr/share/xml/scap/ssg/content/ssg-rhel7-ds.xml

- oscap xccdf remediate --results scan-xccdf-results.xml scan-xccdf-results.xml


OpenSCAP Remediation Review
The review mode enables users to store remediation instructions to a file for further review. The remediation content is not executed during this operation.
To generate remediation instructions in the form of a shell script, run:

-   oscap xccdf generate fix --template urn:xccdf:fix:script:sh --profile xccdf_org.ssgproject.content_profile_rht-ccp --output my-remediation-script.sh /usr/share/xml/scap/ssg/content/ssg-rhel7-ds.xml

Exporting XCCDF Results for the DISA STIG Viewer
OpenSCAP enables you to output your XCCDF results to a format that is compatible with DISA STIG Viewer
To scan a local system for DISA STIG compliance and produce XCCDF results that can be opened in DISA STIG Viewer:

-   oscap xccdf eval --profile xccdf_org.ssgproject.content_profile_stig-rhel7-disa --stig-viewer stig-results.xml /usr/share/xml/scap/ssg/content/ssg-rhel7-ds.xml





License
-------

BSD

Author Information
------------------
Tameika Reed CIE TEAM
