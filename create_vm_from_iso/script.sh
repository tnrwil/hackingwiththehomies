#!/bin/bash

set -x

#sudo yum makecache 
export RHN_USERNAME=$(cat /tmp/file.ini | grep RHN_USERNAME | cut -d'=' -f2)
export RHN_PASSWORD=$(cat /tmp/file.ini | grep RHN_PASSWORD | cut -d'=' -f2)
echo "#########################1"
sudo subscription-manager repos --disable=local-repo
echo "#########################2"
sudo subscription-manager register --username $RHN_USERNAME --password $RHN_PASSWORD
echo "#########################3"
sudo subscription-manager attach --pool=8a85f9916f0fa05d016f1580ede53262
sudo subscription-manager repos --enable rhel-7-server-extras-rpms
sudo yum install ansible -y
echo "#########################4"
#sudo yum --security update-minimal -y
#echo "###############################5"
#
##This did not work
##sudo yum -y update kernel 

#Trying this
#sudo yum -y update kernel kernel-devel

#sudo subscription-manager repos --enable=rhel-7-server-rpms
#sudo subscription-manager repos --enable=rhel-7-server-ansible-2.6-rpms
#sudo yum install -y ansible 
#sudo pip2.7 install jinja2
#sudo pip3 install jinja2
#sudo yum install -y open-vm-tools.x86_64
#sudo yum install -y scap-workbench
#sudo yum install -y openscap-scanner
#sudo yum install -y scap-security-guide
#sudo yum --security update -y
#sudo yum install -y  http://mirror.centos.org/centos/7/extras/x86_64/Packages/container-selinux-2.119.1-1.c57a6f9.el7.noarch.rpm
#sudo systemctl status vmtoolsd.service
#sudo systemctl start vmtoolsd.service
#sudo systemctl enable vmtoolsd.service 
